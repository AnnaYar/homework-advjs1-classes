/*Теоретичне питання

1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

Сенс прототипного наслідування у тому, що об'єкт можна зробити прототипом іншого.
Якщо перший об'єкт є прототипом другого об'єкту, то другий об'єкт успадкує властивість, яка є у першого об'єкту, 
якщо інше не вказано явно. І не потрібно повторювати всю інформацію про другий об'єкт, яку він успадковує від першого.
Перший об'єкт може мати свій прототип. Що складає ланцюг прототипів.


2. Для чого потрібно викликати super() у конструкторі класу - нащадка ?

Super - це ключове слово, яке використовується для виклику методів батьківського класу в дочірньому класі. 
Коли ми розширюємо клас за допомогою наслідування, іноді нам потрібно використовувати або модифікувати поведінку 
батьківського класу, замість того, щоб писати все заново.

    
Завдання
    
Створити клас Employee, у якому будуть такі характеристики - name(ім'я), age (вік), salary (зарплата). 
Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang(список мов).
Для класу Programmer перезапишіть гетер для властивості salary.Нехай він повертає властивість salary,
помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.*/


class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return super.salary*3;
    }
}


const programmerFirst = new Programmer("John", 20, 30000, ["JavaScript", "Java"]);
const programmerSecond = new Programmer("Bob", 25, 35000, ["Python", "C++"]);

console.log(programmerFirst);
console.log(programmerFirst.salary);
console.log(programmerSecond);
console.log(programmerSecond.salary);


